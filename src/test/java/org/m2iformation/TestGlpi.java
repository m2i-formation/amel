package org.m2iformation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

@FixMethodOrder (MethodSorters.NAME_ASCENDING)
public class TestGlpi {
	
	public static ChromeDriver driver;
	public static String nameOrdi;

	@BeforeClass
	public static void setup() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@Test
	public void a_connexion() {
		driver.get("https://demo.glpi-project.org/");
		driver.findElement(By.id("login_name")).sendKeys("admin");
		driver.findElement(By.id("login_password")).sendKeys("admin");
		new Select (driver.findElement(By.name("language"))).selectByVisibleText("Fran�ais");
		driver.findElement(By.xpath("//input[@name='submit' and @value = 'Post']")).click();

	}
	
	@Test
	public void b_creerUnNouveauOrdinateur() {
		driver.findElement(By.linkText("Parc")).click();
		driver.findElement(By.linkText("Ordinateurs")).click();
		driver.findElement(By.linkText("Ajouter")).click();
		driver.findElement(By.linkText("Gabarit vide")).click();
		nameOrdi= "monOrdi";
		driver.findElement(By.name("name")).sendKeys(nameOrdi);
		//new Select (driver.findElement(By.name("users_id_tech"))).selectByVisibleText("glpi");
		driver.findElement(By.name("add")).click();
		driver.findElement(By.xpath("//input[@name='globalsearch']")).sendKeys(nameOrdi);
		driver.findElement(By.name("globalsearchglass")).click();
		
	}
	

}
